class WordTransformation:

    @staticmethod
    def __distance(a, b):
        return len([i for i in range(len(a)) if a[i] != b[i]])

    def __init__(self, words):
        self.graph = {}
        self.words = words

    def __find_shortest_path(self, start, end, path=[]):
        path = path + [start]
        if start == end:
            return path
        if start not in self.graph:
            return None
        shortest = None
        for node in self.graph[start]:
            if node not in path:
                new_path = self.__find_shortest_path(node, end, path)
                if new_path:
                    if not shortest or len(new_path) < len(shortest):
                        shortest = new_path
        return shortest

    def __add_node(self, node):
        if node not in self.graph:
            self.graph[node] = [w for w in self.words if len(w) == len(node) and self.__distance(w, node) == 1]

    def shortest_transformation(self, begin_word, end_word):

        if len(begin_word) != len(end_word):
            raise ValueError('begin_word={} and end_word={} should have same length'.format(begin_word, end_word))

        transformation = []

        if begin_word in self.words:
            self.__add_node(begin_word)

        path = self.__find_shortest_path(begin_word, end_word)

        if path:
            transformation = path
        else:
            for word in self.words:
                if len(word) == len(begin_word):
                    self.__add_node(word)
                    path = self.__find_shortest_path(begin_word, end_word)
                    if path: transformation = path

        return len(transformation) - 1
