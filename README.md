# [Word Transformation](https://discuss.codecademy.com/t/challenge-word-transformation/84306)

## Basic Difficulty
> Given two words (`begin_word` and `end_word`), and a dictionary's word list, find the length of shortest transformation sequence from `begin_word` to `end_word`.

*You may assume*:
* Both `begin_word` and `end_word` are strings of equal length (that is, equal numbers of characters).
`begin_word` and `end_word` are words with all lower-case characters (in ASCII hexadecimal, characters range from `61` to `7A`)
    * Letters cannot be inserted or deleted, only substituted.
* Only one letter can be changed at a time.
* Each intermediate word must exist in the word list.

## Intermediate difficulty
> `begin_word` and `end_word` may now be strings of different lengths.

*You may assume*:
* Your function can now delete and insert letters.


## Hard Difficulty
> Make your submission as efficient as possible.

* Try adding a timing function to your code, and try out different approaches to the problem to find the quickest solution. Also, try to reduce the amount of space you use.
* You may limit your function to check whether the edit distance between `begin_word` and `end_word` is within some threshold or maximum distance of interest, k.
