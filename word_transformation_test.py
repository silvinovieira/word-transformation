from word_transformation import WordTransformation
import unittest


class TestWordTransformation(unittest.TestCase):

    def setUp(self):
        with open('dictionary.txt', 'r') as dictionary:
            words = [word.strip() for word in dictionary]
        self.wt = WordTransformation(words)
        pass

    def test_gate_to_gate(self):
        self.assertEqual(self.wt.shortest_transformation('gate', 'gate'), 0)

    def test_jake_to_jade(self):
        self.assertEqual(self.wt.shortest_transformation('jake', 'jade'), 1)

    def test_humid_to_hemic(self):
        # humid -> humic -> hemic
        self.assertEqual(self.wt.shortest_transformation('humid', 'hemic'), 2)

    def test_mist_to_wary(self):
        # mist -> mitt -> mite -> mate -> maze -> faze -> fade -> wade -> wane -> want -> wart -> wary
        self.assertEqual(self.wt.shortest_transformation('mist', 'wary'), 11)

    def test_wait_to_doll(self):
        # wait -> want -> wane -> wade -> fade -> faze -> maze -> male -> malt -> molt -> moll || dolt -> doll
        self.assertEqual(self.wt.shortest_transformation('wait', 'doll'), 11)

    def test_slug_to_gate(self):
        # not possible
        self.assertEqual(self.wt.shortest_transformation('slug', 'gate'), -1)


if __name__ == '__main__':
    unittest.main()
